<?php
function tentukan_nilai($number)
{
  if ($number==98) {
   echo "Nilai $number => Sangat Baik <br>";
  } else if ($number==76){
   echo "Nilai $number => Baik <br>";
  } else if ($number==67) {
   echo "Nilai $number => Cukup <br>";
  } else if ($number==43) {
   echo "Nilai $number => Kurang <br>";
  } else {
   echo "Nilai tidak cocok";
  }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>